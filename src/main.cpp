#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <boost/filesystem.hpp>
#include <vector>
#include <array>
#include <algorithm>
#include <iostream>

namespace {
  int Brightness( const cv::Vec4b& p )
  {
    return p[0] + p[1] + p[2];
  }
  int Score( const cv::Mat& m, short y, short x )
  {
    return Brightness( m.at<cv::Vec4b>(y,   x)   ) * 3
      +    Brightness( m.at<cv::Vec4b>(y-1, x) ) * 2
      +    Brightness( m.at<cv::Vec4b>(y+1, x) ) * 2
      +    Brightness( m.at<cv::Vec4b>(y,   x-1) ) * 2
      +    Brightness( m.at<cv::Vec4b>(y,   x+1) ) * 2
      +    Brightness( m.at<cv::Vec4b>(y+1, x+1) )
      +    Brightness( m.at<cv::Vec4b>(y-1, x-1) )
      +    Brightness( m.at<cv::Vec4b>(y+1, x-1) )
      +    Brightness( m.at<cv::Vec4b>(y-1, x+1) );
  }
  void ProcessFile( const char* fn )
  {
    std::cout << fn << '\n';
    auto img = cv::imread( fn, -1 );
    std::cout << img.channels() << ' ' << ( img.depth() == CV_8U ) << '\n';
    for ( auto col = 1; col < img.cols - 1; ++col )
    {
      for ( auto row = 1; row < img.rows - 1; ++row )
      {
	auto& pixel = img.at<cv::Vec4b>( row, col );
	if ( pixel[3] == 0 )
	  continue;
	auto score = Score( img, row, col );
	if ( score < 4500 )
	{
	  pixel[3] = 255;
	}
	else if ( score > 5625 )
	{
	  pixel[3] = 0;
	}
      }
    }
    boost::filesystem::path out = "/tmp";
    out /= boost::filesystem::path(fn).filename();
    std::cout << out << '\n';
    cv::imwrite( out.string(), img );
  }
}

int main( int argc, const char* argv[] )
{
  std::for_each( std::next(argv), std::next(argv, argc), &ProcessFile );
}
