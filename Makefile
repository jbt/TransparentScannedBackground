
prepare:= $(shell mkdir bin 2>&- )
srcs:= $(wildcard src/*.cpp ) 
incs:= $(addprefix -I , $(HOME)/.local/include ) 
libdirs:= $(addprefix -L , $(HOME)/.local/lib ) 
libs:= $(addprefix -l , $(addprefix opencv_, highgui imgproc core ) $(addprefix boost_, filesystem system ) ) 
warns:= $(addprefix -W, all extra pedantic error ) 

bin/$(notdir $(abspath . ) ) : $(srcs)
	$(CXX) -o $@ $^ -std=c++11 $(warns) $(incs) $(libdirs) $(libs) 

